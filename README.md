# Modoboa Install notes

[![Modoboa Logo](https://7331.1337.pictures/storage/originals/9d/4a/mail-modoboa-text-sw.png)](https://github.com/modoboa/modoboa)

### Notes to finish modoboa install or debug
#### Installing:
* database urls have to end in `/databasename`
* see below for apache configs for debugging srv and uwsgi
* apache2.4 disliked loading mod_python
* in the examples, we assume the installer is cloned under `/usr/src`  and instance path is `/etc/modoboa/instance`
* if virtualenv fails or smth, you might install to system (or venv) with : `pip install modoboa modoboa_radicale modoboa_amavis modoboa_contacts modoboa_imap_migration modoboa_dmarc modoboa_webmail modoboa_postfix_autoreply modoboa_stats modoboa_pdfcredentials modoboa_sievefilters mysqlclient`

#### Debugging:
 - remember: installer is bound to domain , so only this one (here mydomain.com) is engaged in first place, even if you modify settings→allowed  urls 
 - find <instance_folder>/instance/settings.py and enable `DEBUG=True` temporarily
 - `DJANGO_DEBUG=1 python -Wall manage.py runserver`
 - check with e.g `curl -H 'Host: mydomain.com' http://127.0.0.1:8000/ -kLv` ( to emulate proper domain) or (in apache config) `ProxyPreserveHost On` and `RequestHeader set Host "mydomain.com"`
 - if trouble arises , try `cd ~/instance/;DJANGO_DEBUG=2 strace -f -e trace=network -s 10000  python -Wall manage.py runserver 2>&1 |less` as the modoboa user to see all file handles /network sockets , very often permission errors e.g. on /var/run/dovecot causes empty crypt-scheme dropdown

QuickChart 

| error | solution | hints |
|--|--|--|
| # ProgrammingError at /dashboard/ (1146, "Table 'amavis.msgrcpt' doesn't exist")| `cat /usr/src/modoboa-installer/modoboa_installer/scripts/files/amavis/amavis_mysql_2.10.1.sql\| mysql -u amavis -p amavis` | use amavis passwd and create that user before running installer |
| empty dropdown menu under "default encrypt scheme" | enable $USER access to /var/run/dovecot/config (or add user= and group in apache config for uwsgidaemon or uid= gid= in uwsgi.ini) , enable $ USER access to NSCD socket etc....  you find it via   `cd ~/instance/;DJANGO_DEBUG=2 strace -f -e trace=network -s 10000  python -Wall manage.py runserver 2>&1 |less`  | no help on the internetz  → bug filed here: https://groups.google.com/forum/#!topic/modoboa-users/hIoI8kirZiM |

Once your dovecot and modoboa became friends it should looks like this:
[![N|Solid](./modoboa_scr_scheme.png)](https://modoboa.org/en/)



---


## Apache Configs ( generate certs before !!)
#### Using uwsgi:
```
<VirtualHost *:80>
        ServerName mydomain.com
        ServerAlias www.mydomain.com
        #RewriteRule ^/(.*)$ https://mydomain.com/$1 [L,R=301]
#        DocumentRoot /domains/mydomain.com/www
        RedirectMatch 301 ^(?!/\.well-known/acme-challenge/).* https://mydomain.com$0
        ErrorLog ${APACHE_LOG_DIR}/mydomain.com-error.log
        CustomLog ${APACHE_LOG_DIR}/mydomain.com-access.log combined

</VirtualHost>
<VirtualHost *:443>
#LogLevel info
        ServerName mydomain.com
#        ServerAlias www.mydomain.com

 DocumentRoot /etc/modoboa/instance


#        ProxyPass /media !
#        ProxyPass /sitestatic !
#        ProxyPass / http://127.0.0.1:8000/ connectiontimeout=14400 timeout=14400 Keepalive=On
#        ProxyPassReverse / http://127.0.0.1:8000/
#        RequestHeader set Host "mydomain.com"
#ProxyPreserveHost On
#        RequestHeader set X-Forwarded-Proto "https"
#        SSLProxyEngine On#        SSLProxyVerify none#        SSLProxyCheckPeerCN off#        SSLProxyCheckPeerName off
#        SSLProxyCheckPeerExpire off#        ProxyRequests On#        ProxyPreserveHost On#SetEnv force-proxy-request-1.0 0#SetEnv proxy-nokeepalive 0
  Alias /media/ /etc/modoboa/instance/media/
  <Directory /etc/modoboa/instance/media>
        Require all granted
  </Directory>

  Alias /sitestatic/ /etc/modoboa/instance/sitestatic/
  <Directory /etc/modoboa/instance/sitestatic>
        Require all granted
  </Directory>
  <Directory /etc/modoboa/instance/>
        Require all granted
  </Directory>

  WSGIScriptAlias / /etc/modoboa/instance/instance/wsgi.py
  WSGIDaemonProcess mydomain.com python-path=/etc/modoboa/instance:/etc/modoboa/env/lib/python2.7/site-packages user=modoboa group=dovenull
  WSGIProcessGroup mydomain.com

  # Pass Authorization header to enable API usage:
#  WSGIPassAuthorization On
        ErrorLog    ${APACHE_LOG_DIR}/mydomain.com-error.log
        CustomLog ${APACHE_LOG_DIR}/mydomain.com-access.log combined

        Include /etc/letsencrypt/options-ssl-apache.conf
        SSLCertificateFile /etc/letsencrypt/live/mydomain.com/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/mydomain.com/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf

</VirtualHost>



```

#### (!TESTING) Using django http server (TESTING!) :
```
<VirtualHost *:80>
        ServerName mydomain.com
        ServerAlias www.mydomain.com
        #RewriteRule ^/(.*)$ https://mydomain.com/$1 [L,R=301]
#        DocumentRoot /domains/mydomain.com/www
        RedirectMatch 301 ^(?!/\.well-known/acme-challenge/).* https://mydomain.com$0
        ErrorLog ${APACHE_LOG_DIR}/mydomain.com-error.log
        CustomLog ${APACHE_LOG_DIR}/mydomain.com-access.log combined

</VirtualHost>
<VirtualHost *:443>
LogLevel info
        ServerName mydomain.com
#        ServerAlias www.mydomain.com

 DocumentRoot /etc/modoboa/instance

 # Alias /media/ /etc/modoboa/instance/media/
  <Directory /etc/modoboa/instance/media>
Require all granted
  </Directory>

 # Alias /sitestatic/ /etc/modoboa/instance/sitestatic/
  <Directory /etc/modoboa/instance/sitestatic>
Require all granted
  </Directory>

        ProxyPass /media !
        ProxyPass /sitestatic !
        ProxyPass / http://127.0.0.1:8000/ connectiontimeout=14400 timeout=14400 Keepalive=On
        ProxyPassReverse / http://127.0.0.1:8000/
        RequestHeader set Host "mydomain.com"
ProxyPreserveHost On
        RequestHeader set X-Forwarded-Proto "https"
#        SSLProxyEngine On#        SSLProxyVerify none#        SSLProxyCheckPeerCN off#        SSLProxyCheckPeerName off
#        SSLProxyCheckPeerExpire off#        ProxyRequests On#        ProxyPreserveHost On#SetEnv force-proxy-request-1.0 0#SetEnv proxy-nokeepalive 0

#  WSGIScriptAlias / /etc/modoboa/instance/instance/wsgi.py
#  WSGIDaemonProcess mydomain.com python-path=/etc/modoboa/instance:/etc/modoboa/env/lib/python2.7/site-packages
#  WSGIProcessGroup mydomain.com

  # Pass Authorization header to enable API usage:
#  WSGIPassAuthorization On
        ErrorLog    ${APACHE_LOG_DIR}/mydomain.com-error.log
        CustomLog ${APACHE_LOG_DIR}/mydomain.com-access.log combined

        Include /etc/letsencrypt/options-ssl-apache.conf
        SSLCertificateFile /etc/letsencrypt/live/mydomain.com/fullchain.pem
        SSLCertificateKeyFile /etc/letsencrypt/live/mydomain.com/privkey.pem
        Include /etc/letsencrypt/options-ssl-apache.conf

</VirtualHost>

```

---


What you hopefully see:
[![N|Solid](./modoboa_scr.png)](https://modoboa.org/en/)


## props to : 
| name | fancy link |
|--|--|
| dillinger | [![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger) |

https://modwsgi.readthedocs.io/en/develop/user-guides/debugging-techniques.html
https://stackoverflow.com/questions/37723215/how-to-manage-mod-wsgi-logs-in-apache

## no props to:
#### modoboa docs:
| [webserver config ] | (https://modoboa.readthedocs.io/en/1.1.2/integration/web_servers.html) |
|--|--|
| [amavis config] | (https://github.com/modoboa/modoboa-amavis/blob/master/docs/install.rst) |


---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/modoboa-sample-install/README.md/logo.jpg" width="480" height="270"/></div></a>
